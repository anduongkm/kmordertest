<?
//set session time, 5 hours
$session_lifetime = 3600 * 10;
session_set_cookie_params ($session_lifetime);
session_start();
//session_set_cookie_params(0);
//session_start();
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
<title>Kool Menu – The Evolution of Food Service Menus</title>
<meta name="keyword" content="local restaurant, hotel, best menu, e-menu, eat, drink, food, coffee, bar, tea, best restaurant, pos, order, take out, eat inside, food delivery, good meal">
<meta name="description" content="Kool Menu is an evolution of Food and Drink Service Menus. A personalized menu experience linked directly to your kitchen – available from any mobile device.">
<link href="style_menu.css?v<?= time();?>" rel="stylesheet" type="text/css" />
<link href="js/css/smoothness/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css"/>
<!--script src="js/jquery.lint.js" type="text/javascript" charset="utf-8"></script-->
<link rel="stylesheet" href="js/prettyPhoto/css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
<link rel="shortcut icon" href="icon.png" />
<script src="js/jquery-2.1.0.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/prettyPhoto/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery-migrate-1.2.1.js"></script>
<script src="js/jquery.tooltipster.js"></script>
<link rel="stylesheet" href="spinningwheel.css" type="text/css" media="all" />
<script type="text/javascript" src="js/spinningwheel-min.js"></script>

<script src="js/jquery.ui.totop.js"></script>
<script src="js/touchTouch.jquery.js"></script>
<link rel="stylesheet" href="css/touchTouch.css">
<script>

jQuery(document).ready(function() {

var language = jQuery("#language").val();
  // expand or collapse My order list
  jQuery(".myorder_expand").hide();
  //toggle
  jQuery(".myorder").click(function()
  {
    jQuery(this).next(".myorder_expand").slideToggle(500);
  });
  // hide or show a Category
  jQuery(".categoryLink").click(function()
  {
	//jQuery(".myorder_expand").hide();
    var categoryLink = jQuery(this).next().val();
	//console.log(categoryLink);
	jQuery(".category").hide();
	jQuery("#"+categoryLink).show();
	jQuery(".categoryLink").parent().attr("class","");
	jQuery(this).parent().attr("class","current");
	jQuery(".tabs li").toggle();
	jQuery(".tabs li:first-child").show();
  });
  
  // hide or show a Category
  jQuery("#chooseMenu").click(function()
  {
	jQuery(".tabs li").toggle();
	jQuery(".tabs li:first-child").show();
  });
  // To top
  jQuery().UItoTop({ easingType: 'easeOutQuart' });
               jQuery('.tooltip').tooltipster();
		   
	jQuery('.pictureslider').touchTouch();
	// Pretty Photo function
	/*jQuery("area[rel^='prettyPhoto']").prettyPhoto();
				
	jQuery(".gallery a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal',theme:'light_square',slideshow:40000, autoplay_slideshow: true});
	jQuery(".gallery:gt(0) a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'fast',slideshow:10000, hideflash: true});
	
	jQuery("#custom_content a[rel^='prettyPhoto']:first").prettyPhoto({
		custom_markup: '<div id="map_canvas" style="width:260px; height:265px"></div>',
		changepicturecallback: function(){ initialize(); }
	});

	jQuery("#custom_content a[rel^='prettyPhoto']:last").prettyPhoto({
		custom_markup: '<div id="bsap_1259344" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6"></div><div id="bsap_1237859" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6" style="height:260px"></div><div id="bsap_1251710" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6"></div>',
		changepicturecallback: function(){ _bsap.exec(); }
	});
	*/
	
	//$.mobile.zoom.disable();
	
	//check the type of table
	//jQuery("#tableID").change(function()
	jQuery(document).on('change', '#tableID', function()
	{	
	var tableID=jQuery(this).val();
	var tableType = jQuery("#tableID_"+tableID).val();
	//console.log(tableType);
	if(tableType==1)
	{
		var htmlAppend = '<div><input style="width:300px;height:40px;margin-top:19px;" placeholder="';
		if(language=="VN") htmlAppend+=  "&nbsp;Mã số"; else {htmlAppend+= '&nbsp;Table'; htmlAppend+= "'s"; htmlAppend+= ' Pin';}
		htmlAppend+= '" name="pin" type="number" id="pin" value="" /><!--<span style="color:red;margin-left:5px;">*</span>--></div>';
		jQuery("#customerInfo").html(htmlAppend);
				
		if(language=="VN")
		jQuery("#notificationWords").html('<p style="font-size:16px;margin-left:10px;">Xin vui lòng chọn bàn và nhập vào mã số để đăng nhập.</p>');
		else jQuery("#notificationWords").html('<p style="font-size:16px;margin-left:10px;">Please identify your table, and select a PIN (password) for this visit.</p>');
	}
	if(tableType!=1)
	{
	
		var htmlAppend = '';
		
		jQuery("#customerInfo").html(htmlAppend);
		if(language=="VN")
		jQuery("#notificationWords").html('');
		else jQuery("#notificationWords").html('');
	}
	
	/*
	if(tableType==2)
	{
	
		var htmlAppend = '<div id="tablePin"><input style="width:300px;height:40px;margin-top:19px;" placeholder="';
		if(language=="VN") htmlAppend+=  "&nbsp;Số điện thoại"; else htmlAppend+=  "&nbsp;Your Phone #";
		htmlAppend+= '" name="customerPhone" type="number" id="customerPhone" value="" /><!--<span style="color:red;margin-left:5px;">*</span>--></div>';
		htmlAppend+= '<div id="customerNote"><input style="width:300px;height:40px;margin-top:19px;" placeholder="';
		if(language=="VN") htmlAppend+=  "&nbsp;Ghi chú"; else htmlAppend+=  "&nbsp;Note";
		htmlAppend+= '" name="orderNote" type="text" id="orderNote" value="" /><!--<span style="color:red;margin-left:5px;"></span>--></div>';
		
		jQuery("#customerInfo").html(htmlAppend);
		if(language=="VN")
		jQuery("#notificationWords").html('<p style="font-size:16px;margin-left:10px;">Xin vui lòng điền đầy đủ thông tin bên trên, chúng tôi sẽ liên hệ với bạn để báo tình trạng đơn hàng khi cần.</p>');
		else jQuery("#notificationWords").html('<p style="font-size:16px;margin-left:10px;">Please let us know the phone number associated with this order.</p>');
	}
	
	if(tableType==3)
	{
		var htmlAppend = '<div id="tablePin"><input style="width:300px;height:40px;margin-top:19px;" placeholder="';
		if(language=="VN") htmlAppend+=  "&nbsp;Số điện thoại"; else htmlAppend+=  "&nbsp;Your Phone #";
		htmlAppend+= '" name="customerPhone" type="number" id="customerPhone" value="" /><!--<span style="color:red;margin-left:5px;">*</span>--></div>';
		htmlAppend+= '<div id="noteAddress"><input style="width:300px;height:40px;margin-top:19px;" placeholder="';
		if(language=="VN") htmlAppend+=  "&nbsp;Địa chỉ"; else htmlAppend+=  "&nbsp;Your Address";
		htmlAppend+= '" name="orderNote" type="text" id="orderNote" value="" /><!--<span style="color:red;margin-left:5px;"></span>--></div>';
		
		jQuery("#customerInfo").html(htmlAppend);
	
		if(language=="VN")
		jQuery("#notificationWords").html('<p style="font-size:16px;margin-left:10px;">Xin vui lòng điền đầy đủ thông tin bên trên, chúng tôi sẽ liên hệ với bạn để báo tình trạng đơn hàng khi cần.</p>');
		else jQuery("#notificationWords").html('<p style="font-size:16px;margin-left:10px;">Please let us know the phone number and address associated with this order.</p>');
	}
	*/
	/*$.ajax
	({
	type: "POST",
	url: "includes/ajax/ajax_checkTableType.php",
	data: ({tableID:tableID
				}),
	cache: false,
	success: function(html)
	{
	jQuery("#customerInfo").html(html);
	var customerPhone = jQuery('#customerPhone').val();
	
	
	//console.log(customerPhone);
	if(customerPhone == 0)
		{
		jQuery('#customerPhone').val('');
		
		if(language=="VN")
		jQuery("#notificationWords").html('<p style="font-size:16px;margin-left:10px;">Xin vui lòng nhập đầy đủ thông tin vào các ô bên trên, chúng tôi sẽ liên hệ với bạn để báo tình trạng đơn hàng khi cần.</p>');
		else jQuery("#notificationWords").html('<p style="font-size:16px;margin-left:10px;">Please identify your phone for this visit.</p>');
		}
	else {
		if(language=="VN")
		jQuery("#notificationWords").html('<p style="font-size:16px;margin-left:10px;">Xin vui lòng chọn bàn và nhập vào mã số để đăng nhập.</p>');
		else jQuery("#notificationWords").html('<p style="font-size:16px;margin-left:10px;">Please identify your table, and select a PIN (password) for this visit.</p>');
		}
	} 
	});*/

	});	

	//callback handler for form submit
	jQuery("#myOrderForm").submit(function(e)
	{
		//var postData = jQuery(this).serializeArray();
		
		var tableID=jQuery("#tableID").val();
		var tableType = jQuery("#tableID_"+tableID).val();
		var customerID=jQuery("#customerID").val();
		var orderNumber=jQuery("#orderNumber").val();
		var lineNumber=jQuery("#lineNumber").val();
		var iteamSize=jQuery("#iteamSize").val();
		var tableNumber = jQuery("#tableNumber").val();
		var customerName = jQuery("#customerName").val();
		var submitvalid;
		submitvalid=1;
		jQuery('#confirm_order').hide();
		
		if(tableType!=1 &&  (customerName=="" || customerName === undefined || customerName === null)) {submitvalid=0;} 
		if(submitvalid==1) {
		jQuery.ajax(
		{
			url: "includes/order_process.php",
			type: "POST",
			data: jQuery(this).serialize(),
			success:function(data, textStatus, jqXHR) 
			{
				//data: return data from server
				
				jQuery("#myOrderData").html(data);
				//updateOrderNum();
				var myorderNum = jQuery("#orderNumber").val();
				jQuery("#myorderNum").text('('+tableNumber+') '+myorderNum);
				if(language=="VN") alert("Yêu cầu của bạn đã được gởi thành công!"); else alert("Your order was sent");
				//jQuery('#confirm_order').show();
				jQuery('#tablePin').hide();
				jQuery('#customerNote').hide();
				jQuery('#noteAddress').hide();
			},
			error: function(jqXHR, textStatus, errorThrown) 
			{
				//if fails      
			}
		});
		}
		
		else if(submitvalid==0) {
		if(language=="VN") alert("Vui lòng nhập tên hoặc số điện thoại, để xác thật yêu cầu đặt hàng."); 
		else alert("Please let us know your name or your phone number associated with this order");
		jQuery('#confirm_order').show();
		// apend customer information requested boxes
		if(tableType==2 || tableType==4)
	{
	
		var htmlAppend = '<div id="tablePin" style="float:left; margin-right:10px;"><input autofocus style="width:300px;height:40px;margin-top:19px;font-size: 22px;" placeholder="';
		if(language=="VN") htmlAppend+=  "&nbsp;Tên/Số điện thoại"; else htmlAppend+=  "&nbsp;Your name/Your Phone #";
		htmlAppend+= '" name="customerName" type="text" id="customerName" value="" /></div>';
		htmlAppend+= '<div id="customerNote"><input style="width:300px;height:40px;margin-top:19px;font-size: 22px;" placeholder="';
		if(language=="VN") htmlAppend+=  "&nbsp;Ghi chú"; else htmlAppend+=  "&nbsp;Note";
		htmlAppend+= '" name="orderNote" type="text" id="orderNote" value="" /></div>';
		
		jQuery("#customerInfoHere").html(htmlAppend);
	}
	
	if(tableType==3)
	{
		var htmlAppend = '<div id="tablePin" style="float:left; margin-right:10px;"><input autofocus style="width:300px;height:40px;margin-top:19px;font-size: 22px;" placeholder="';
		if(language=="VN") htmlAppend+=  "&nbsp;Tên/Số điện thoại"; else htmlAppend+=  "&nbsp;Your name/Your Phone #";
		htmlAppend+= '" name="customerName" type="text" id="customerName" value="" /></div>';
		htmlAppend+= '<div id="noteAddress"><input style="width:300px;height:40px;margin-top:19px;font-size: 22px;" placeholder="';
		if(language=="VN") htmlAppend+=  "&nbsp;Địa chỉ"; else htmlAppend+=  "&nbsp;Your Address";
		htmlAppend+= '" name="orderNote" type="text" id="orderNote" value="" /></div>';
		
		jQuery("#customerInfoHere").html(htmlAppend);
	}
		}
		console.log(customerName,tableType,submitvalid);
		e.preventDefault(); //STOP default action
		//e.unbind(); //unbind. to stop multiple form submit.
		
	});
	 
	//jQuery("#myOrderForm").submit();
				  
});			
			


</script>

<?php

$useragent=$_SERVER['HTTP_USER_AGENT'];

if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))

{
	$isMobile=1;
}
if($_SERVER['HTTP_USER_AGENT'] == 'Mozilla/5.0(iPad; U; CPU iPhone OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B314 Safari/531.21.10') 
{
    $isMobile=2;
	//echo '<style>body{width:900px;margin:0 auto;}</style>';
}

if($isMobile==1)
 {
 echo '<link href="style_mobile.css?v'.time().'" rel="stylesheet" type="text/css" />';
}
if($isMobile!=2)
 {
 echo '<link href="style_tablet.css'.time().'" rel="stylesheet" type="text/css" />';
}
else echo '<style>body{width:960px;margin:0 auto;}</style>';

?>

</head>
<?  
date_default_timezone_set("America/New_York");
$date=date("Y-m-d h:i:sa");
require_once ("../includes/initialize_logic.php");
require_once ("../includes/classes/mysql_ultimate.php");  


include_once("googleanalytics.php");
require_once('languagecode.php');
$ipAddress = $_SERVER['REMOTE_ADDR'];
//Log Out
if($_GET["action"]=="logout" && isset($_SESSION["customerID"]))
{
	//reset everything
	$_SESSION["customerID"] = "";
	$_SESSION["tableID"]="";
	$_SESSION['customerName']="";
	//$_SESSION['randNum'] = "";
	$_SESSION['ipAddress'] = "";	
	$customerID = "";
	$tableID = "";
	$tableValid = 0;
	$userValid = 0;
	$_SESSION['userName']="";
	$_SESSION["userID"] = "";
}

if(isset($_POST['userName']) && $_POST['userName']!="")
{	
	$query = 'select * from users where userName="'.$_POST['userName'].'" and passWord="'.$_POST['passWord'].'"';
	//echo $query;
	// check username and password
	if ($db->Query($query)) 
			{	
				while ($resultRow = $db->Row()) 
				{					
					$customerID = $resultRow->customerID;
					$_SESSION['userName'] = $resultRow->userName;
					$_SESSION['ipAddress'] = $ipAddress;
					$_SESSION["customerID"] = $customerID;
					$_SESSION["userID"] = $resultRow->id;
					$userValid = 1;
				}
			}
		
		
}

//Log In
elseif(isset($_POST['customerID'])) 
	{
	//If more than device login to a table, gives it a random number to split the bill
	if(!isset($_SESSION['randNum']))
	{
		$_SESSION['randNum'] = (rand(1000,9999));
	}
	
	$_SESSION['customerID'] = $_POST['customerID'];
	$_SESSION['tableID'] = $_POST['tableID'];
	$customerID = $_POST['customerID'];
	$tableID = $_POST['tableID'];
	$tablePin = $_POST['pin'];
	$orderNote = $_POST['orderNote'];
	//if(isset($_SESSION['userName'])) $userValid ==1;
	if(isset($tableID))
	{
		$query10 = 'select id from tableList where customerID='.$customerID.' and id='.$tableID.' and pin="'.$tablePin.'"';
		//echo $query10;
		$tableValid = 0;
		
		// if order type is For Here, customer has to enter Pin
		if ($db->Query($query10) && $_POST['pin']!="") 
				{	
					while ($resultRow = $db->Row()) 
					{					
						if(isset($resultRow->id)) $tableValid = 1;
					}
				}
		// if order type is Take Out, customer has to enter Phone
		else {
			$tableValid = 1;
		}
		/*else {
			if(isset($_POST['customerPhone']) && $_POST['customerPhone'] !="") $tableValid = 1; 
			else $tableValid = 0;
		}*/
	}
	$_SESSION['ipAddress'] = $ipAddress;
	if(isset($_POST['customerPhone'])) {$_SESSION['customerPhone'] = $_POST['customerPhone'];}	
	if(isset($_POST['customerName'])) {$_SESSION['customerName'] = $_POST['customerName'];}	
}
elseif($_GET['rid']>0) 
	{	
	//If more than device login to a table, gives it a random number to split the bill
	if(!isset($_SESSION['randNum']))
	{
		$_SESSION['randNum'] = (rand(1000,9999));
	}
	$_SESSION['customerID'] = $_GET['rid'];	
	$customerID = $_SESSION['customerID'];
	$tableValid = 1;
}

else {
	//If more than device login to a table, gives it a random number to split the bill
	if(!isset($_SESSION['randNum']))
	{
		$_SESSION['randNum'] = (rand(1000,9999));
	}
	$customerID = $_SESSION["customerID"];
	if(isset($_GET['tableID'])) $tableID = $_GET['tableID'];
	elseif(isset($_SESSION["tableID"])) $tableID = $_SESSION["tableID"];	
	$tableValid = 1;
	if(isset($_SESSION['userName'])) $userValid =1;
}

?>

<body>
<!--
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
-->
	<?
	if($userValid ==1 or (isset($customerID) && $customerID!="" && $tableID!=0 && $tableValid==1 &&($ipAddress==$_SESSION['ipAddress'])))
	{
	require_once ("includes/order.php");
	}
	else 
	{
	require_once ("includes/login_screen.php");
	}
	?>
	<footer>
		<p id="footerWord"><? if($ln=="VN") echo "Thiết kế bởi Kool Menu Inc."; else echo "Powered by Kool Menu Inc."; ?></p>
	</footer>
</body>

</html>