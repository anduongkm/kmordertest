<?
session_set_cookie_params(0);
session_start();
?>
<?
require_once('../geoplugin.php');
$geoplugin = new geoPlugin();
$geoplugin->locate();
// create a variable for the country code
$var_country_code = $geoplugin->countryCode;
// redirect based on country code:
if ($var_country_code != "US") {
$ln = "VN";
//echo "test location:".$ln;
echo '<input  name="language" id="language"  type="hidden"  value="'.$ln.'"/>';
}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
<title>Kool Menu – The Evolution of Food Service Menus</title>
<meta name="keyword" content="local restaurant, hotel, best menu, e-menu, eat, drink, food, coffee, bar, tea, best restaurant, pos, order, take out, eat inside, food delivery, good meal">
<meta name="description" content="Kool Menu is an evolution of Food and Drink Service Menus. A personalized menu experience linked directly to your kitchen – available from any mobile device.">
<link href="style_menu.css?v<?= time();?>" rel="stylesheet" type="text/css" />
<link href="../js/css/smoothness/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css"/>
<!--script src="../js/jquery.lint.js" type="text/javascript" charset="utf-8"></script-->
<link rel="stylesheet" href="../js/prettyPhoto/css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
<link rel="shortcut icon" href="icon.png" />
<script src="../js/jquery-2.1.0.min.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/prettyPhoto/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
<script src="../js/jquery-migrate-1.2.1.js"></script>
<script src="../js/jquery.tooltipster.js"></script>
<link rel="stylesheet" href="spinningwheel.css" type="text/css" media="all" />
<script type="text/javascript" src="../js/spinningwheel-min.js"></script>

<script src="../js/jquery.ui.totop.js"></script>
<script src="../js/touchTouch.jquery.js"></script>
<link rel="stylesheet" href="../css/touchTouch.css">
<script>

jQuery(document).ready(function() {
  // expand or collapse My order list
  jQuery(".myorder_expand").hide();
  //toggle
  jQuery(".myorder").click(function()
  {
    jQuery(this).next(".myorder_expand").slideToggle(500);
  });
  // hide or show a Category
  jQuery(".categoryLink").click(function()
  {
	//jQuery(".myorder_expand").hide();
    var categoryLink = $(this).next().val();
	//console.log(categoryLink);
	jQuery(".category").hide();
	jQuery("#"+categoryLink).show();
	$(".categoryLink").parent().attr("class","");
	$(this).parent().attr("class","current");
	jQuery(".tabs li").toggle();
	$(".tabs li:first-child").show();
  });
  
  // hide or show a Category
  jQuery("#chooseMenu").click(function()
  {
	jQuery(".tabs li").toggle();
	$(".tabs li:first-child").show();
  });
  // To top
  $().UItoTop({ easingType: 'easeOutQuart' });
               $('.tooltip').tooltipster();
		   
	$('.pictureslider').touchTouch();
	// Pretty Photo function
	/*$("area[rel^='prettyPhoto']").prettyPhoto();
				
	$(".gallery a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal',theme:'light_square',slideshow:40000, autoplay_slideshow: true});
	$(".gallery:gt(0) a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'fast',slideshow:10000, hideflash: true});
	
	$("#custom_content a[rel^='prettyPhoto']:first").prettyPhoto({
		custom_markup: '<div id="map_canvas" style="width:260px; height:265px"></div>',
		changepicturecallback: function(){ initialize(); }
	});

	$("#custom_content a[rel^='prettyPhoto']:last").prettyPhoto({
		custom_markup: '<div id="bsap_1259344" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6"></div><div id="bsap_1237859" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6" style="height:260px"></div><div id="bsap_1251710" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6"></div>',
		changepicturecallback: function(){ _bsap.exec(); }
	});
	*/
	
	//$.mobile.zoom.disable();
	
	//check the type of table
	//$("#tableID").change(function()
	jQuery(document).on('change', '#tableID', function()
	{
	var tableID=$(this).val();
	var tableType = $("#tableID_"+tableID).val();
	var language = $("#language").val();
	//console.log(tableType);
	if(tableType==1)
	{
		var htmlAppend = '<div><input style="min-width:300px;height:30px;" placeholder="';
		if(language="VN") htmlAppend+=  "Mã số"; else {htmlAppend+= 'Table'; htmlAppend+= "'s"; htmlAppend+= ' Pin';}
		htmlAppend+= '" name="pin" type="number" id="pin" value="" /><span style="color:red;margin-left:5px;">*</span></div>';
		$("#customerInfo").html(htmlAppend);
				
		if(language="VN")
		$("#notificationWords").html('<p style="font-size:16px;margin-left:10px;">Xin vui lòng chọn bàn và nhập vào mã số để đăng nhập.</p>');
		else $("#notificationWords").html('<p style="font-size:16px;margin-left:10px;">Please identify your table, and select a PIN (password) for this visit.</p>');
	}
	
	if(tableType==2)
	{
	
		var htmlAppend = '<div id="tablePin"><input style="min-width:300px;height:30px;" placeholder="';
		if(language="VN") htmlAppend+=  "Số điện thoại"; else htmlAppend+=  "Your Phone #";
		htmlAppend+= '" name="customerPhone" type="number" id="customerPhone" value="" /><span style="color:red;margin-left:5px;">*</span></div>';
		htmlAppend+= '<div id="customerNote"><input style="min-width:300px;height:30px;margin-top:19px;" placeholder="';
		if(language="VN") htmlAppend+=  "Ghi chú"; else htmlAppend+=  "Note";
		htmlAppend+= '" name="orderNote" type="text" id="orderNote" value="" /><span style="color:red;margin-left:5px;"></span></div>';
		
		$("#customerInfo").html(htmlAppend);
		if(language="VN")
		$("#notificationWords").html('<p style="font-size:16px;margin-left:10px;">Xin vui lòng nhập đầy đủ thông tin vào các ô bên trên, chúng tôi sẽ liên hệ với bạn để báo tình trạng đơn hàng khi cần.</p>');
		else $("#notificationWords").html('<p style="font-size:16px;margin-left:10px;">Please identify your phone for this visit.</p>');
	}
	
	if(tableType==3)
	{
		var htmlAppend = '<div id="tablePin"><input style="min-width:300px;height:30px;" placeholder="';
		if(language="VN") htmlAppend+=  "Số điện thoại"; else htmlAppend+=  "Your Phone #";
		htmlAppend+= '" name="customerPhone" type="number" id="customerPhone" value="" /><span style="color:red;margin-left:5px;">*</span></div>';
		htmlAppend+= '<div id="noteAddress"><input style="min-width:300px;height:30px;" placeholder="';
		if(language="VN") htmlAppend+=  "Địa chỉ"; else htmlAppend+=  "Address";
		htmlAppend+= '" name="orderNote" type="text" id="orderNote" value="" /><span style="color:red;margin-left:5px;"></span></div>';
		
		$("#customerInfo").html(htmlAppend);
	
		if(language="VN")
		$("#notificationWords").html('<p style="font-size:16px;margin-left:10px;">Xin vui lòng nhập đầy đủ thông tin vào các ô bên trên, chúng tôi sẽ liên hệ với bạn để báo tình trạng đơn hàng khi cần.</p>');
		else $("#notificationWords").html('<p style="font-size:16px;margin-left:10px;">Please identify your phone for this visit.</p>');
	}
	
	/*$.ajax
	({
	type: "POST",
	url: "includes/ajax/ajax_checkTableType.php",
	data: ({tableID:tableID
				}),
	cache: false,
	success: function(html)
	{
	$("#customerInfo").html(html);
	var customerPhone = $('#customerPhone').val();
	
	
	//console.log(customerPhone);
	if(customerPhone == 0)
		{
		$('#customerPhone').val('');
		
		if(language="VN")
		$("#notificationWords").html('<p style="font-size:16px;margin-left:10px;">Xin vui lòng nhập đầy đủ thông tin vào các ô bên trên, chúng tôi sẽ liên hệ với bạn để báo tình trạng đơn hàng khi cần.</p>');
		else $("#notificationWords").html('<p style="font-size:16px;margin-left:10px;">Please identify your phone for this visit.</p>');
		}
	else {
		if(language="VN")
		$("#notificationWords").html('<p style="font-size:16px;margin-left:10px;">Xin vui lòng chọn bàn và nhập vào mã số để đăng nhập.</p>');
		else $("#notificationWords").html('<p style="font-size:16px;margin-left:10px;">Please identify your table, and select a PIN (password) for this visit.</p>');
		}
	} 
	});*/

	});	

	//callback handler for form submit
	$("#myOrderForm").submit(function(e)
	{
		//var postData = $(this).serializeArray();
		var language = $("#language").val();
		var tableID=$("#tableID").val();
		var customerID=$("#customerID").val();
		var orderNumber=$("#orderNumber").val();
		var lineNumber=$("#lineNumber").val();
		var iteamSize=$("#iteamSize").val();
		var tableNumber = $("#tableNumber").val();
		$('#confirm_order').hide();
		$.ajax(
		{
			url: "includes/order_process.php",
			type: "POST",
			data: $(this).serialize(),
			success:function(data, textStatus, jqXHR) 
			{
				//data: return data from server
				//console.log(data);
				$("#myOrderData").html(data);
				//updateOrderNum();
				var myorderNum = $("#orderNumber").val();
				$("#myorderNum").text('('+tableNumber+') '+myorderNum);
				if(language=="VN")alert("Yêu cầu của bạn đã được gởi thành công!"); else alert("Your order was sent");
			},
			error: function(jqXHR, textStatus, errorThrown) 
			{
				//if fails      
			}
		});
		e.preventDefault(); //STOP default action
		e.unbind(); //unbind. to stop multiple form submit.
		
	});
	 
	//$("#myOrderForm").submit();
				  
});			
			


</script>

<?php

$useragent=$_SERVER['HTTP_USER_AGENT'];

if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))

{
	$isMobile=1;
}
if($_SERVER['HTTP_USER_AGENT'] == 'Mozilla/5.0(iPad; U; CPU iPhone OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B314 Safari/531.21.10') 
{
    $isMobile=2;
	echo '<style>body{width:900px;margin:0 auto;}</style>';
}

if($isMobile==1)
 {
 echo '<link href="style_mobile.css" rel="stylesheet" type="text/css" />';
}
else echo '<style>body{width:900px;margin:0 auto;}</style>';

?>

</head>
<?  
date_default_timezone_set("America/New_York");
$date=date("Y-m-d h:i:sa");
require_once ("../../includes/initialize_logic.php");
require_once ("../../includes/classes/mysql_ultimate.php");  


include_once("../googleanalytics.php");

$ipAddress = $_SERVER['REMOTE_ADDR'];
//Log Out
if($_GET["action"]=="logout" && isset($_SESSION["customerID"]))
{
	//reset everything
	$_SESSION["customerID"] = "";
	$_SESSION["tableID"]="";
	$_SESSION['customerPhone']="";
	//$_SESSION['randNum'] = "";
	$_SESSION['ipAddress'] = "";	
	$customerID = "";
	$tableID = "";
	$tableValid = 0;
}
//Log In
if(isset($_POST['customerID'])) 
	{
	//If more than device login to a table, gives it a random number to split the bill
	if(!isset($_SESSION['randNum']))
	{
		$_SESSION['randNum'] = (rand(1000,9999));
	}
	
	$_SESSION['customerID'] = $_POST['customerID'];
	$_SESSION['tableID'] = $_POST['tableID'];
	$customerID = $_POST['customerID'];
	$tableID = $_POST['tableID'];
	$tablePin = $_POST['pin'];
	$orderNote = $_POST['orderNote'];
	if(isset($tableID))
	{
		$query10 = 'select id from tableList where customerID='.$customerID.' and id='.$tableID.' and pin="'.$tablePin.'"';
		//echo $query10;
		$tableValid = 0;
		
		// if order type is For Here, customer has to enter Pin
		if ($db->Query($query10) && $_POST['pin']!="") 
				{	
					while ($resultRow = $db->Row()) 
					{					
						if(isset($resultRow->id)) $tableValid = 1;
					}
				}
		// if order type is Take Out, customer has to enter Phone
		else {
			if(isset($_POST['customerPhone']) && $_POST['customerPhone'] !="") $tableValid = 1; 
			else $tableValid = 0;
		}
	}
	$_SESSION['ipAddress'] = $ipAddress;
	if(isset($_POST['customerPhone'])) {$_SESSION['customerPhone'] = $_POST['customerPhone'];}	
	if(isset($_POST['customerName'])) {$_SESSION['customerName'] = $_POST['customerName'];}	
}

?>

<body>
	<?
	if(isset($customerID) && $customerID!="" && $tableID!=0 && $tableValid==1 &&($ipAddress==$_SESSION['ipAddress']))
	{
	require_once ("../includes/order.php?userID=");
	}
	else 
	{
	require_once ("login.php");
	}
	?>
	<footer>
		<p id="footerWord">Powered by Kool Menu Inc.</p>
	</footer>
</body>

</html>