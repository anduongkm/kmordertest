
//©2010-2011 Erobo Software 
//Visit http://www.erobo.net for free scripts and libraries
//1-05-2010 - added drag capabilities.
//1-22-2010 - added option for fade in effect.

//Credits:
//http://www.xul.fr/javascript/ ->style sheet, gradient function, main method logic
//http://www.webreference.com/programming/javascript/mk/column2/ [Mark Kahn] ->basic drag drop functions

//fade in global
var undoGradient = false;
//lightbox content explicit
var lighboxContent = "";
Number.prototype.format = function(n, x, s, c) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = this.toFixed(Math.max(0, ~~n));

    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};

function gradient(id, level){
  var box = document.getElementById(id);
  box.style.opacity = level;
  box.style.MozOpacity = level;
  box.style.KhtmlOpacity = level;
  box.style.filter = "alpha(opacity=" + level * 100 + ")";
  if(undoGradient == false){
    box.style.display="block";
  }
  return;
}

function fadein(id){
  var level = 0;

  while(level <= 1)
  {
    setTimeout( "gradient('" + id + "'," + level + ")", (level* 1000) + 10);
    level += 0.01;
  }
}

// Open the lightbox
function openbox(formtitle, actions, price, itemID, itemName, orderNumberValue, fadin, boxwidth, boxheight, boxtitlewidth, boxtitleheight){

  var screenWidthSpaceAvail, iniHeight, screenHeightSpaceAvail, alt_ua_ie;
  var currHeight, disableSelections, yScroll, windowHeight;
  var language = jQuery("#language").val();
  
  if(navigator.userAgent.toLowerCase().indexOf("msie")!=-1) {
    screenWidthSpaceAvail = document.documentElement.clientWidth;
    iniHeight = document.documentElement.clientHeight;
    currHeight = document.documentElement.scrollTop;
    alt_ua_ie = true;
  }else if(navigator.userAgent.toLowerCase().indexOf("firefox")!=-1) {
    screenWidthSpaceAvail = window.innerWidth;
    iniHeight = window.innerHeight;
    currHeight = window.pageYOffset;
    alt_ua_ie = false;
  }else if(navigator.userAgent.toLowerCase().indexOf("chrome")!=-1) {
    screenWidthSpaceAvail = window.innerWidth;
    iniHeight = window.innerHeight;
    currHeight = window.pageYOffset;
    alt_ua_ie = false;
  }else if(navigator.userAgent.toLowerCase().indexOf("safari")!=-1) {
    screenWidthSpaceAvail = window.innerWidth;
    iniHeight = window.innerHeight;
    currHeight = window.pageYOffset;
    alt_ua_ie = false;
  }else if(navigator.userAgent.toLowerCase().indexOf("opera")!=-1) {
    screenWidthSpaceAvail = window.innerWidth;
    iniHeight = window.innerHeight;
    currHeight = window.pageYOffset;
    alt_ua_ie = true;
  }else {
    screenWidthSpaceAvail = document.documentElement.clientWidth;
    iniHeight = document.documentElement.clientHeight;
    currHeight = document.documentElement.scrollTop;
    alt_ua_ie = true;
  }
  
  //determining the height code from quirksmode.org
  if (window.innerHeight && window.scrollMaxY) {	
    yScroll = window.innerHeight + window.scrollMaxY;
  } else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
    yScroll = document.body.scrollHeight;
  } else { // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
    yScroll = document.body.offsetHeight;
  }

  if (self.innerHeight) {	// all except Explorer
    windowHeight = self.innerHeight;
  } else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
    windowHeight = document.documentElement.clientHeight;
  } else if (document.body) { // other Explorers
    windowHeight = document.body.clientHeight;
  }	

  // for small pages with total height less then height of the viewport

  if(yScroll < windowHeight){
    screenHeightSpaceAvail = windowHeight;
  } else { 
    screenHeightSpaceAvail = yScroll;
  }

  //end of quirksmode.org code for determining height

  undoGradient = false; //reset global for gradient
  var boxContainer = document.getElementById('boxcontainer'); 
  var box = null;
  
  if(document.getElementById('box') == null){
    box = document.createElement('div');
    box.setAttribute("id", "box");
  }else{
    box = document.getElementById('box');
  }

  box.style.width = boxwidth + "px";
  box.style.height = boxheight + "px";
  box.style.position = "absolute";
  box.style.top = currHeight + (iniHeight / 2) - (boxheight / 2) + "px";
  box.style.left = (screenWidthSpaceAvail / 2) - (boxwidth / 2) + "px";
  
  if(lighboxContent == ""){
  lighboxContent = boxContainer.innerHTML;
  box.innerHTML = "<div width=\"100%\">" + lighboxContent + "<\/div>";
  boxContainer.innerHTML = "";
  
  }else{
    box.innerHTML = lighboxContent;
  }
  
  if(document.getElementById('box') == null){
    document.body.appendChild(box);
  }
 
  box.style.zIndex = "9990";

  if(document.getElementById("filter") == null){
    var mydiv = document.createElement('div');
    mydiv.setAttribute("id", "filter");
    document.body.appendChild(mydiv);
  }

  if (navigator.userAgent.toLowerCase().indexOf("chrome") != -1) {
    screenHeightSpaceAvail = 0; //chrome wont show proper dimensions when scroll past certain point
  }

  document.getElementById('filter').style.display='block';
  document.getElementById('filter').style.width= screenWidthSpaceAvail + "px";
  document.getElementById('filter').style.height= screenHeightSpaceAvail + "px";
  document.getElementById('filter').style.opacity =  0.5;
  document.getElementById('filter').style.filter =  "alpha(opacity=50)";
  document.getElementById('filter').style.zIndex = "9980";

  var btitle = document.getElementById('boxtitle');
  btitle.style.width = boxtitlewidth + "px";
  btitle.style.height = boxtitleheight + "px";
  btitle.innerHTML = formtitle;
  btitle.style.cursor = "move";

  document.onmousemove = mouseMove;
  document.onmouseup   = mouseUp;

  var dragObject  = null;
  var mouseOffset = null;

  function getMouseOffset(target, ev){
    ev = ev || window.event;
    var docPos    = getPosition(target);
    var mousePos  = mouseCoords(ev);

    return {x:mousePos.x - docPos.x, y:mousePos.y - docPos.y};
  }

  function getPosition(e){
    var left = 0;
    var top  = 0;

    while (e.offsetParent){
      left += e.offsetLeft;
      top  += e.offsetTop;
      e     = e.offsetParent;
    }

    left += e.offsetLeft;
    top  += e.offsetTop;

    return {x:left, y:top};

  }
  
  function mouseCoords(ev){

    if(ev.pageX || ev.pageY){
      return {x:ev.pageX, y:ev.pageY};
    }

    return {

      x:ev.clientX + document.body.scrollLeft - document.body.clientLeft,
      y:ev.clientY + document.body.scrollTop  - document.body.clientTop

	};
  }

  function mouseMove(ev){

    ev  = ev || window.event;
    var mousePos = mouseCoords(ev);

    if(dragObject){

      dragObject.style.position = 'absolute';
      dragObject.style.top      = (mousePos.y - mouseOffset.y) + "px";
      dragObject.style.left     = (mousePos.x - mouseOffset.x) + "px";

      return false;
    }
  }

  function mouseUp(){

    dragObject = null;

    if (typeof box.onselectstart!="undefined"){
	  box.onselectstart=null;
	} else if (typeof box.style.MozUserSelect!="undefined"){
	  box.style.MozUserSelect="text"
	}
	  
	disableSelections = false

  }

  function makeDraggable(item){

    if(!item) return;

    item.onmousedown = function(ev){
      dragObject  = this;
      mouseOffset = getMouseOffset(this, ev);
      return false;
    }
  }
  
  btitle.onmousedown = instantiateDrag;
  
  function instantiateDrag(){

    if(disableSelections == false){
      if (typeof box.onselectstart!="undefined"){
        box.onselectstart=function(){return false}
      } else if (typeof box.style.MozUserSelect!="undefined"){
        box.style.MozUserSelect="none"
      }
    }
    
    makeDraggable(box)
  
    disableSelections = true;
    
  }
  
  box.onclick = function(){
	dragObject = null;
	document.getElementById('box').onmousedown = null;
  }
  
  if(fadin)
  {
   gradient("box", 0);
   fadein("box");
  }
  else
  {   
    box.style.display='block';
  }
	
	if(actions=="payment")
 {
  if(language=="VN")
  jQuery('.totalh2').html('Tổng: VND '+total.format(2, 3, ',', '.'));
  else jQuery('.totalh2').html('Total: $'+total.format(2, 3, ',', '.'));
  jQuery("#orderNumberValue").val(orderNumberValue);
  jQuery("#orderID").val(orderID);
	//console.log(total); 
	//console.log(orderNumberValue);
  }
  
  if(actions=="itemSize")
 {
  if(language=="VN")
  jQuery('.totalh2').html('Chọn size');
  else jQuery('.totalh2').html("Item's size");
  
  var htmlvalue;
  var tablecode;
  htmlvalue = '<input type="hidden" name="lightboxAction" value="itemSize" checked id="lightboxAction"><input type="hidden" name="orderNumberValue" value="'+orderNumberValue+'" checked id="orderNumberValue"><input type="hidden" name="itemID" value="'+itemID+'" checked id="itemID">';
  htmlvalue += '<br><br><b><span style="color:red;" id="itemName">'+itemName+'</span></b><br>';
  if(language=="VN")  
  {  
  htmlvalue += '<b>Giá: <span style="color:red;" id="lightBoxPrice">'+price+'<input id="lightBoxPriceValue" type="hidden" value="'+price+'" /></span></b><br><b>Chọn size:</b>';
  }
  else htmlvalue += '<b>Price: $<span style="color:red;" id="lightBoxPrice">'+price+'<input id="lightBoxPriceValue" type="hidden" value="'+price+'" /></span></b><br><b>Please choose size:</b>';
  htmlvalue+='<br>';
  tablecode= jQuery('#hasItemSizeDiv_'+itemID).html();
  //remove the html code at the hasItemSizeDiv_
  jQuery('#hasItemSizeDiv_'+itemID).html('');
  htmlvalue += '<div id="hasItemSizeDivLightBox_'+itemID+'">'+tablecode+'</div>';
  
  if(language=="VN")
  htmlvalue += '<br><br><p><span id="ajax_status"></span><a href="#tabs" class="submitButton">Chọn</a><a href="#tabs" onClick="closebox()" class="cancelButton">Hủy</a></p> ';
  else 
  htmlvalue +='<br><br><p><span id="ajax_status"></span><a href="#tabs" class="submitButton">Order</a><a href="#tabs" onClick="closebox()" class="cancelButton"> Cancel</a></p> ';
  
 
  
  jQuery('#lightbox_form').html(htmlvalue);
  
  }
}

function dragBoxBody(){
  var box1 = document.getElementById('box'); 
  box1.style.left = 1  + "px";
}

// Close the lightbox
function closebox(){
   undoGradient = true;
   document.getElementById('box').style.display='none';
   document.getElementById('filter').style.display='none';
   var itemID = jQuery("#itemID").val();
   var hasItemSizeDivLightBox = jQuery('#hasItemSizeDivLightBox_'+itemID).html();
   jQuery('#hasItemSizeDiv_'+itemID).html(hasItemSizeDivLightBox);
}

